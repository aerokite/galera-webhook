docker-build: docker-stop
	docker build -t aerokite/codership docker

docker-run: docker-build
	docker run -d -p 45671:4567 -p 33061:3306 -e TYPE=primary --name=node1 -v /home/shahriar/go/src/github.com/aerokite/codership/docker/config/node1.ini:/etc/mysql/conf.d/my.cnf aerokite/codership
	docker run -d -p 45672:4567 -p 33062:3306 --name=node2 -v /home/shahriar/go/src/github.com/aerokite/codership/docker/config/node2.ini:/etc/mysql/conf.d/my.cnf aerokite/codership
	docker run -d -p 45673:4567 -p 33063:3306 --name=node3 -v /home/shahriar/go/src/github.com/aerokite/codership/docker/config/node3.ini:/etc/mysql/conf.d/my.cnf aerokite/codership

docker-stop:
	docker stop node1; docker rm node1 | true
	docker stop node2; docker rm node2 | true
	docker stop node3; docker rm node3 | true


stop-node2:
	docker exec node2 bash -- service mysql stop

start-node2:
	docker exec node2 bash -- service mysql start


stop-node3:
	docker exec node3 bash -- service mysql stop

start-node3:
	docker exec node3 bash -- service mysql start
