package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os/user"
	"path/filepath"

	_ "github.com/go-sql-driver/mysql"
)

type Config struct {
	Nodes      []string `json:"nodes"`
	Connection struct {
		Username string `json:"username"`
		Password string `json:"password"`
		Database string `json:"database"`
	} `json:"connection"`
	Webhook struct {
		Port int `json:"port"`
	} `json:"webhook"`
}

var config Config

func init() {
	u, err := user.Current()
	if err != nil {
		log.Fatal(err)
	}
	data, err := ioutil.ReadFile(filepath.Join(u.HomeDir, ".galera", "config.json"))
	if err != nil {
		log.Fatal(err)
	}
	if err := json.Unmarshal(data, &config); err != nil {
		log.Fatal(err)
	}
}

func main() {
	http.HandleFunc("/", func(writer http.ResponseWriter, request *http.Request) {
		if request.Method == http.MethodGet {
			for _, addr := range config.Nodes {
				showNodeStatus(addr, writer)
			}
		}
	})

	http.ListenAndServe(fmt.Sprintf(":%d", config.Webhook.Port), nil)
}

func showNodeStatus(addr string, w http.ResponseWriter) (n int, err error) {
	conn := config.Connection
	db, err := sql.Open("mysql", fmt.Sprintf("%s:%s@tcp(%s)/%s", conn.Username, conn.Password, addr, conn.Database))
	if err != nil {
		return fmt.Fprintln(w, fmt.Sprintf("Address: [%s]; Failed to connect to database; Error: %v", addr, err))
	}
	res, err := db.Query("SHOW VARIABLES LIKE 'wsrep_node_name';")
	if err != nil {
		return fmt.Fprintln(w, fmt.Sprintf("Address: [%s]; Failed to query database; Error: %v", addr, err))
	}
	var col1, col2 []byte
	for res.Next() {
		res.Scan(&col1, &col2)
		if name := string(col2); name != "" {
			return fmt.Fprintln(w, fmt.Sprintf("Address: [%s]; Name: %s; Status: Running", addr, name))
		}
	}
	return fmt.Fprintln(w, fmt.Sprintf("Address: [%s]; Status: Running; Message: failed to get node name", addr))
}
