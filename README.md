# Galera Cluster Status Webhook

This project is used to display node status of a Galera MySQL cluster.

Contains three folder:

- docker: This is to run Galera cluster
- webhook: Run webhook with fixed cluster information
- binary: Run webhook with configuration

## Docker

```bash
# This will create Galera cluster with three node.
# But you need to update, docker/config/node*.ini to match your docker IP. In my case, its `172.17.0.1`
# Update "wsrep_cluster_address"
make docker-run
```


This will create three node. Following Port will be exposed

- Node1: `{IP}:45671`, `{IP}:33061`
- Node2: `{IP}:45672`, `{IP}:33062`
- Node3: `{IP}:45673`, `{IP}:33063`

Port `4567_` is used for cluster and `3306_` is used as mysql client

## Webhook

Run webhook to show running node list in browser.
But you need to update default settings in code.

```go
for _, a := range []string{"172.17.0.1:33061", "172.17.0.1:33062", "172.17.0.1:33063"} {
```

Update IP in above part.

Go to your browser @ 127.0.0.1:3333

## Binary

This one is similar as Webhook package. The only difference is this program reads settings from configuration file.

Configuration File:

```json
{
  "nodes": ["172.17.0.1:33061", "172.17.0.1:33062", "172.17.0.1:33063"],
  "connection": {
    "username": "root",
    "password": "1234",
    "database": "test"
  },
  "webhook": {
    "port": 3333
  }
}
```

Update `nodes` and `webhook.port`. But if you want to check with another cluster, you can change `webhook.connection` too.

Keep this config file in `$HOME/.galera/config.json`

# Output

```
Address: [172.17.0.1:33061]; Name: node1; Status: Running
Address: [172.17.0.1:33062]; Name: node2; Status: Running
Address: [172.17.0.1:33063]; Name: node3; Status: Running
```

Stop node2 using. You will see as bellow

```
Address: [172.17.0.1:33061]; Name: node1; Status: Running
Address: [172.17.0.1:33062]; Failed to query database; Error: dial tcp 172.17.0.1:33062: connect: connection refused
Address: [172.17.0.1:33063]; Name: node3; Status: Running
```

I can skip displaying error message for node2. But Its better to let user know.