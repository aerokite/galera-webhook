#!/bin/bash

mysql_install_db --explicit_defaults_for_timestamp

chown -R mysql:mysql /var/lib/mysql /var/run/mysqld

cat >/etc/mysql/conf.d/cluster.cnf <<EOL
[mysqld]
wsrep_cluster_name="$cluster_name"
wsrep_node_name="$node_name"
wsrep_cluster_address="gcomm://$ip_list"
EOL


if [ "$TYPE" == "primary" ]; then
    service mysql start --wsrep-new-cluster --init-file="/setup.sql"
else
    service mysql start
fi

sleep infinity
