-- What's done in this file shouldn't be replicated
		--  or products like mysql-fabric won't work
SET @@SESSION.SQL_LOG_BIN=0;

DELETE FROM mysql.user ;
CREATE USER 'root'@'%' IDENTIFIED BY '1234' ;
GRANT ALL ON *.* TO 'root'@'%' WITH GRANT OPTION ;
DROP DATABASE IF EXISTS test;
CREATE DATABASE IF NOT EXISTS test;

CREATE USER 'kite'@'%' IDENTIFIED BY '1234';
CREATE USER 'kite'@'localhost' IDENTIFIED BY '1234' ;

GRANT ALL ON `test`.* TO 'kite'@'%';
GRANT ALL ON `test`.* TO 'kite'@'localhost' ;

FLUSH PRIVILEGES;
