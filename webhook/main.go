package main

import (
	"database/sql"
	"fmt"
	"net"
	"net/http"
	"os/exec"
	"strconv"
	"strings"

	_ "github.com/go-sql-driver/mysql"
	"sync"
	"time"
)

func dockerClean(name string) {
	exec.Command("docker", "stop", name).Run()
	exec.Command("docker", "rm", name).Run()
}

func pullDocker(writer http.ResponseWriter) error {
	//fmt.Fprintln(writer, "Pulling docker image")
	cmd := exec.Command("docker", "pull", "aerokite/codership:latest")
	//cmd.Stdout = writer
	if err := cmd.Run(); err != nil {
		return fmt.Errorf("failed to pull docker image. Error: %v", err)
	}
	return nil
}

func runDocker(clusterName, nodeName, nodeType, nodePort, clientPort string, ips []string, wg *sync.WaitGroup) error {
	defer wg.Done()
	dockerClean(nodeName)

	cmd := exec.Command(
		"docker",
		"run", "-d",
		"-p", fmt.Sprintf("%s:4567", nodePort),
		"-p", fmt.Sprintf("%s:3306", clientPort),
		"-e", fmt.Sprintf("cluster_name=%s", clusterName),
		"-e", fmt.Sprintf("node_name=%s", nodeName),
		"-e", fmt.Sprintf("TYPE=%s", nodeType),
		"-e", fmt.Sprintf("ip_list=%s", strings.Join(ips, ",")),
		fmt.Sprintf("--name=%s", nodeName),
		"aerokite/codership:latest",
	)

	//cmd.Stdout = writer
	if err := cmd.Run(); err != nil {
		return fmt.Errorf(`failed to run docker for node "%s". Error: %v`, nodeName, err)
	}

	waitForRunning(fmt.Sprintf("%s:%s", NodeIP, clientPort))
	return nil
}

func getFreePort() (string, error) {
	l, err := net.Listen("tcp", ":0")
	if err != nil {
		return "", err
	}
	defer l.Close()
	return strings.Split(l.Addr().String(), "[::]:")[1], nil
}

const (
	cluster_name = "cluster_name"
	node_count   = "node_count"
)

type NodeInfo struct {
	NodeType   string
	NodeName   string
	NodePort   string
	ClientPort string
}

var clusterDic = map[string][]NodeInfo{}

var NodeIP = "172.17.0.1"

func main() {

	clusterDic = make(map[string][]NodeInfo)

	http.HandleFunc("/start", func(writer http.ResponseWriter, request *http.Request) {
		if request.Method == http.MethodGet {
			clusterName := request.URL.Query().Get(cluster_name)
			if clusterName == "" {
				http.Error(writer, "cluster_name not provided", 400)
				return
			}

			if _, found := clusterDic[clusterName]; found {
				http.Error(writer, fmt.Sprintf(`cluster "%s" is already exists.`, clusterName), 400)
				return
			} else {
				clusterDic[clusterName] = make([]NodeInfo, 0)
			}

			nodeCountString := request.URL.Query().Get(node_count)
			nodeCount := 3
			if nodeCountString != "" {
				var err error
				if nodeCount, err = strconv.Atoi(nodeCountString); err != nil {
					http.Error(writer, err.Error(), 400)
					return
				}
			}

			if nodeCount < 3 {
				nodeCount = 3
			}

			nodeList := clusterDic[clusterName]
			nodeIp := make([]string, 0)
			for i := 0; i < nodeCount; i++ {
				nodePort, err := getFreePort()
				if err != nil {
					http.Error(writer, err.Error(), 400)
					return
				}
				clientPort, err := getFreePort()
				if err != nil {
					http.Error(writer, err.Error(), 400)
					return
				}

				nodeType := "node"
				if i == 0 {
					nodeType = "primary"
				}

				nodeName := fmt.Sprintf("%s-%d", clusterName, i)
				nodeList = append(nodeList, NodeInfo{
					NodeType:   nodeType,
					NodeName:   nodeName,
					NodePort:   nodePort,
					ClientPort: clientPort,
				})

				nodeIp = append(nodeIp, fmt.Sprintf("%s:%s", NodeIP, nodePort))
			}
			clusterDic[clusterName] = nodeList

			if err := pullDocker(writer); err != nil {
				http.Error(writer, err.Error(), 500)
			}

			wg := &sync.WaitGroup{}
			for i, _ := range nodeList {
				n := nodeList[i]
				wg.Add(1)
				//fmt.Fprintln(writer, fmt.Sprintf(`Creating node "%s"`, n.NodeName))
				go func() {
					if err := runDocker(clusterName, n.NodeName, n.NodeType, n.NodePort, n.ClientPort, nodeIp, wg); err != nil {
						http.Error(writer, err.Error(), 500)
						return
					}
				}()
			}

			wg.Wait()
			for _, a := range nodeList {
				showNodeStatus(fmt.Sprintf("%s:%s", NodeIP, a.ClientPort), writer)
			}
		}
	})

	http.HandleFunc("/stop", func(writer http.ResponseWriter, request *http.Request) {
		if request.Method == http.MethodGet {
			clusterName := request.URL.Query().Get(cluster_name)
			if clusterName == "" {
				http.Error(writer, "cluster_name not provided", 400)
				return
			}

			defer delete(clusterDic, clusterName)

			nodeList, found := clusterDic[clusterName]
			if !found {
				http.Error(writer, fmt.Sprintf(`Cluster "%s" not found`, clusterName), 404)
				return
			}

			wg := &sync.WaitGroup{}
			for i, _ := range nodeList {
				n := nodeList[i]
				fmt.Fprintln(writer, fmt.Sprintf(`Deleting node "%s"`, n.NodeName))
				wg.Add(1)

				go func() {
					dockerClean(n.NodeName)
					wg.Done()
				}()
			}
			wg.Wait()
		}
	})

	http.HandleFunc("/show", func(writer http.ResponseWriter, request *http.Request) {
		if request.Method == http.MethodGet {
			clusterName := request.URL.Query().Get(cluster_name)
			if clusterName == "" {
				http.Error(writer, "cluster_name not provided", 400)
				return
			}

			nodeList, found := clusterDic[clusterName]
			if !found {
				http.Error(writer, fmt.Sprintf(`Cluster "%s" not found`, clusterName), 404)
				return
			}
			for _, a := range nodeList {
				showNodeStatus(fmt.Sprintf("%s:%s", NodeIP, a.ClientPort), writer)
			}
		}
	})
	http.ListenAndServe(":3333", nil)
}

func waitForRunning(addr string) {
	db, _ := sql.Open("mysql", fmt.Sprintf("root:1234@tcp(%s)/test", addr))
	for true {
		if db.Ping() != nil {
			time.Sleep(time.Second * 5)
		} else {
			break
		}
	}
}

func showNodeStatus(addr string, w http.ResponseWriter) (n int, err error) {
	db, err := sql.Open("mysql", fmt.Sprintf("root:1234@tcp(%s)/test", addr))
	if err != nil {
		return fmt.Fprintln(w, fmt.Sprintf("Address: [%s]; Failed to connect to database; Error: %v", addr, err))
	}
	res, err := db.Query("SHOW VARIABLES LIKE 'wsrep_node_name';")
	if err != nil {
		return fmt.Fprintln(w, fmt.Sprintf("Address: [%s]; Failed to query database; Error: %v", addr, err))
	}
	var col1, col2 []byte
	for res.Next() {
		res.Scan(&col1, &col2)
		if name := string(col2); name != "" {
			return fmt.Fprintln(w, fmt.Sprintf("Address: [%s]; Name: %s; Status: Running", addr, name))
		}
	}
	return fmt.Fprintln(w, fmt.Sprintf("Address: [%s]; Status: Running; Message: failed to get node name", addr))
}
